$PBExportHeader$w_iniciar.srw
forward
global type w_iniciar from window
end type
type phl_imagem from picturehyperlink within w_iniciar
end type
end forward

global type w_iniciar from window
integer width = 2971
integer height = 1820
boolean titlebar = true
string title = "Prova de Conceito"
string menuname = "m_base"
boolean controlmenu = true
boolean minbox = true
boolean maxbox = true
boolean resizable = true
windowstate windowstate = maximized!
long backcolor = 67108864
string icon = "Application5!"
boolean center = true
phl_imagem phl_imagem
end type
global w_iniciar w_iniciar

on w_iniciar.create
if this.MenuName = "m_base" then this.MenuID = create m_base
this.phl_imagem=create phl_imagem
this.Control[]={this.phl_imagem}
end on

on w_iniciar.destroy
if IsValid(MenuID) then destroy(MenuID)
destroy(this.phl_imagem)
end on

event resize;phl_imagem.Width = This.Width 
phl_imagem.height = This.height 
end event

type phl_imagem from picturehyperlink within w_iniciar
integer width = 2295
integer height = 1216
string pointer = "HyperLink!"
string picturename = "C:\TCCGabi\imagens\R.jpg"
boolean focusrectangle = false
end type


$PBExportHeader$w_cad_cliente.srw
forward
global type w_cad_cliente from w_cadastro
end type
type dw_cliente from datawindow within w_cad_cliente
end type
type gb_1 from groupbox within w_cad_cliente
end type
end forward

global type w_cad_cliente from w_cadastro
integer width = 3273
integer height = 1168
string title = "Cadastro de Clientes"
dw_cliente dw_cliente
gb_1 gb_1
end type
global w_cad_cliente w_cad_cliente

on w_cad_cliente.create
int iCurrent
call super::create
this.dw_cliente=create dw_cliente
this.gb_1=create gb_1
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_cliente
this.Control[iCurrent+2]=this.gb_1
end on

on w_cad_cliente.destroy
call super::destroy
destroy(this.dw_cliente)
destroy(this.gb_1)
end on

event open;call super::open;dw_cliente.SetTransObject(SQLCA)

end event

type cb_ok from w_cadastro`cb_ok within w_cad_cliente
integer x = 2368
integer y = 912
end type

event cb_ok::clicked;call super::clicked;datawindow ldw_Gravar[]
ldw_Gravar[1] = dw_cliente

f_update(ldw_Gravar)
end event

type cb_cancelar from w_cadastro`cb_cancelar within w_cad_cliente
integer x = 2830
integer y = 912
end type

type sle_busca from w_cadastro`sle_busca within w_cad_cliente
integer y = 64
end type

type pb_busca from w_cadastro`pb_busca within w_cad_cliente
end type

event pb_busca::clicked;call super::clicked;Long ll_Cliente
String ls_Nome

ll_Cliente = Long(sle_busca.Text)

SELECT
	IDCLIENTE,
	NOME
INTO
	:ll_Cliente,
	:ls_Nome
FROM
	public.cliente
WHERE
	IDCLIENTE = :ll_Cliente
USING SQLCA;

If ll_Cliente = 0 Then
	MessageBox('Cadastro', 'Nenhum cliente encontrado!')
Else	
	st_busca.Text = ls_Nome
End if
end event

type st_busca from w_cadastro`st_busca within w_cad_cliente
integer x = 608
integer y = 88
integer width = 1915
end type

type cb_inseri from w_cadastro`cb_inseri within w_cad_cliente
integer x = 2976
end type

event cb_inseri::clicked;call super::clicked;dw_cliente.Reset()
dw_cliente.InsertRow(0)
end event

type cb_1 from w_cadastro`cb_1 within w_cad_cliente
integer x = 2587
end type

event cb_1::clicked;call super::clicked;dw_cliente.Retrieve(Long(sle_busca.Text))
end event

type gb_busca from w_cadastro`gb_busca within w_cad_cliente
integer width = 3200
end type

type dw_cliente from datawindow within w_cad_cliente
integer x = 37
integer y = 296
integer width = 3131
integer height = 540
integer taborder = 20
string title = "none"
string dataobject = "d_cad_cliente"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type gb_1 from groupbox within w_cad_cliente
integer y = 220
integer width = 3218
integer height = 664
integer taborder = 10
integer textsize = -8
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 67108864
string text = " Cadastro de Clientes"
end type


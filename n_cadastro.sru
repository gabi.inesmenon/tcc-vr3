$PBExportHeader$n_cadastro.sru
forward
global type n_cadastro from nonvisualobject
end type
end forward

global type n_cadastro from nonvisualobject
end type
global n_cadastro n_cadastro

type variables

end variables

forward prototypes
public function boolean of_insere_estoque (integer idempresa, integer idproduto, decimal qtd)
public function boolean of_atualiza_estoque (integer idempresa, integer idproduto, decimal qtd)
end prototypes

public function boolean of_insere_estoque (integer idempresa, integer idproduto, decimal qtd);Long ll_Row

datastore lds_Saldo
lds_Saldo = Create datastore
lds_Saldo.Dataobject = 'd_estoque_saldo'
lds_Saldo.SetTransObject(SQLCA)

If lds_Saldo.Retrieve(idempresa, idproduto) > 0 Then
	lds_Saldo.SetItem(1, 'qtdproduto', lds_Saldo.GetItemNumber(1, 'qtdproduto') + qtd)
Else
	ll_Row = lds_Saldo.InsertRow(0)
	lds_Saldo.SetItem(ll_Row, 'idempresa', idempresa)
	lds_Saldo.SetItem(ll_Row, 'idproduto', idproduto)
	lds_Saldo.SetItem(ll_Row, 'qtdproduto', qtd)
End if
lds_Saldo.update()

Return true
end function

public function boolean of_atualiza_estoque (integer idempresa, integer idproduto, decimal qtd);Long ll_Row

datastore lds_Saldo
lds_Saldo = Create datastore
lds_Saldo.Dataobject = 'd_estoque_saldo'
lds_Saldo.SetTransObject(SQLCA)

If lds_Saldo.Retrieve(idempresa, idproduto) > 0 Then
	lds_Saldo.SetItem(1, 'qtdproduto', lds_Saldo.GetItemNumber(1, 'qtdproduto') - qtd)
Else
	ll_Row = lds_Saldo.InsertRow(0)
	lds_Saldo.SetItem(ll_Row, 'idempresa', idempresa)
	lds_Saldo.SetItem(ll_Row, 'idproduto', idproduto)
	lds_Saldo.SetItem(ll_Row, 'qtdproduto', qtd * -1)
End if
lds_Saldo.update()

Return true
end function

on n_cadastro.create
call super::create
TriggerEvent( this, "constructor" )
end on

on n_cadastro.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on


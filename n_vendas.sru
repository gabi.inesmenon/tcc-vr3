$PBExportHeader$n_vendas.sru
forward
global type n_vendas from nonvisualobject
end type
end forward

global type n_vendas from nonvisualobject
end type
global n_vendas n_vendas

type variables

end variables

forward prototypes
public function boolean of_insere_estoque (integer idempresa, integer idproduto, decimal qtd)
public function boolean of_atualiza_estoque (integer idempresa, integer idproduto, decimal qtd)
end prototypes

public function boolean of_insere_estoque (integer idempresa, integer idproduto, decimal qtd);Long ll_Row

n_compras ln_compras
ln_compras = Create n_compras

datastore lds_Saldo
lds_Saldo = Create datastore
lds_Saldo.Dataobject = 'd_estoque_saldo'
lds_Saldo.SetTransObject(SQLCA)

If lds_Saldo.Retrieve(idempresa, idproduto) > 0 Then
	lds_Saldo.SetItem(1, 'qtdproduto', lds_Saldo.GetItemNumber(1, 'qtdproduto') + qtd)
Else
	ll_Row = lds_Saldo.InsertRow(0)
	lds_Saldo.SetItem(ll_Row, 'idempresa', idempresa)
	lds_Saldo.SetItem(ll_Row, 'idproduto', idproduto)
	lds_Saldo.SetItem(ll_Row, 'qtdproduto', qtd)
End if
lds_Saldo.update()

ln_compras.of_atualiza_estoque(idempresa, idproduto, qtd)

Return true
end function

public function boolean of_atualiza_estoque (integer idempresa, integer idproduto, decimal qtd);Long ll_Row

datastore lds_Saldo
lds_Saldo = Create datastore
lds_Saldo.Dataobject = 'd_estoque_saldo'
lds_Saldo.SetTransObject(SQLCA)

If lds_Saldo.Retrieve(idempresa, idproduto) > 0 Then
	lds_Saldo.SetItem(1, 'qtdproduto', lds_Saldo.GetItemNumber(1, 'qtdproduto') - qtd)
Else
	ll_Row = lds_Saldo.InsertRow(0)
	lds_Saldo.SetItem(ll_Row, 'idempresa', idempresa)
	lds_Saldo.SetItem(ll_Row, 'idproduto', idproduto)
	lds_Saldo.SetItem(ll_Row, 'qtdproduto', qtd * -1)
End if
lds_Saldo.update()

Return true
end function

on n_vendas.create
call super::create
TriggerEvent( this, "constructor" )
end on

on n_vendas.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on


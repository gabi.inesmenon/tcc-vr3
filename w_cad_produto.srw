$PBExportHeader$w_cad_produto.srw
forward
global type w_cad_produto from w_cadastro
end type
type dw_prod from datawindow within w_cad_produto
end type
type gb_1 from groupbox within w_cad_produto
end type
end forward

global type w_cad_produto from w_cadastro
integer width = 2569
integer height = 1064
string title = "Cadastro de Produto"
dw_prod dw_prod
gb_1 gb_1
end type
global w_cad_produto w_cad_produto

on w_cad_produto.create
int iCurrent
call super::create
this.dw_prod=create dw_prod
this.gb_1=create gb_1
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_prod
this.Control[iCurrent+2]=this.gb_1
end on

on w_cad_produto.destroy
call super::destroy
destroy(this.dw_prod)
destroy(this.gb_1)
end on

event open;call super::open;dw_prod.SetTransObject(SQLCA)

end event

type cb_ok from w_cadastro`cb_ok within w_cad_produto
integer x = 1627
integer y = 832
end type

event cb_ok::clicked;call super::clicked;datawindow ldw_Gravar[]
ldw_Gravar[1] = dw_prod

f_update(ldw_Gravar)
end event

type cb_cancelar from w_cadastro`cb_cancelar within w_cad_produto
integer x = 2107
integer y = 832
end type

type sle_busca from w_cadastro`sle_busca within w_cad_produto
end type

type pb_busca from w_cadastro`pb_busca within w_cad_produto
integer y = 64
end type

event pb_busca::clicked;call super::clicked;Long ll_Prod
String ls_Nome

ll_Prod = Long(sle_busca.Text)

SELECT
	IDPRODUTO,
	DESCRICAO
INTO
	:ll_Prod,
	:ls_Nome
FROM
	public.produto
WHERE
	IDPRODUTO = :ll_Prod
USING SQLCA;

If ll_Prod = 0 Then
	MessageBox('Cadastro', 'Nenhum produto encontrado!')
Else	
	st_busca.Text = ls_Nome
End if
end event

type st_busca from w_cadastro`st_busca within w_cad_produto
integer width = 1266
end type

type cb_inseri from w_cadastro`cb_inseri within w_cad_produto
integer x = 2281
integer y = 68
end type

event cb_inseri::clicked;call super::clicked;dw_prod.Reset()
dw_prod.InsertRow(0)
end event

type cb_1 from w_cadastro`cb_1 within w_cad_produto
integer x = 1902
integer y = 68
end type

event cb_1::clicked;call super::clicked;dw_prod.Retrieve(Long(sle_busca.Text))
end event

type gb_busca from w_cadastro`gb_busca within w_cad_produto
integer width = 2501
end type

type dw_prod from datawindow within w_cad_produto
integer x = 59
integer y = 308
integer width = 2418
integer height = 476
integer taborder = 10
boolean bringtotop = true
string title = "none"
string dataobject = "d_cad_produto"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type gb_1 from groupbox within w_cad_produto
integer x = 23
integer y = 220
integer width = 2487
integer height = 588
integer taborder = 20
integer textsize = -8
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 67108864
string text = " Cadastro de Produto "
end type

